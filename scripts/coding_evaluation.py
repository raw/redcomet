import cPickle

import numpy as np
import matplotlib.pyplot as plt

import reedsolo

from redcomet import coding, rcartag

DATA_BITS = 12

def convert_bytearray_to_64(byte_buffer):
    result = np.int64(0)
    k = 36
    for i in xrange(k-1, -1, -1):
        byte_index = (i + 4) / 8
        bit_index = ((k - 1) - i) % 8
        result |= ((byte_buffer[byte_index] >> bit_index) & 1) << (k-1-i)
    return result

def convert_64_to_bytearray(self, arg):
    byte_buffer = bytearray([0]*5)
    k = 36
    for i in xrange(k-1, -1, -1):
        byte_index = (i + 4) / 8
        bit_index = ((k - 1) - i) % 8
        byte_buffer[byte_index] |= ((arg >> (k-1-i)) & 1) << bit_index
    return byte_buffer

def generate_codes(codes):
    rt = rcartag.RedCometARTag()
    rt.duplicate_mask = 223
    all_codes_and_rot = []
    for code in codes:
        for rotation in xrange(4):
            all_codes_and_rot.append((code, rotation))

    code_results = []
    for code, rotation in all_codes_and_rot:
        byte_data = rt.rs_encode(code)
        # Convert to mat to rotate then convert back
        mat_data = rt.convert_bytearray_to_mat(byte_data)
        for i in xrange(rotation):
            mat_data = rt.rotate_mat_cw(mat_data)
        byte_data = rt.convert_mat_to_bytearray(mat_data)
        # Convert to 64 bin for far easier hamming distancing
        bin_data = convert_bytearray_to_64(byte_data)
        code_results.append([code, rotation, bin_data])

    return code_results

def generate_all_codes():
    codes = range(1 << DATA_BITS)
    return generate_codes(codes)

def visualize_histogram():
    filename = "histogram.pkl"
    with open(filename, "rb") as f:
        hist_dict = cPickle.load(f)
    # Convert the hist dict to numpy array for convenient display
    hist = np.zeros(max(hist_dict.keys()) + 1)
    for k, v in hist_dict.iteritems():
        hist[k] = v

    #plt.vlines(hist, 0, len(hist) + 1)
    plt.bar(np.arange(len(hist)), hist)
    plt.xlabel("Hamming Distance")
    plt.ylabel("Occurences")
    #plt.xticks(np.arange(len(hist)))
    plt.savefig("minimum_hist.png", bbox_inches="tight")
    plt.show()

def get_bad_codes(minimum_distance=5):
    filename = "min_distances.pkl"
    with open(filename, "rb") as f:
        distances = cPickle.load(f)
    bad_codes = []
    for code_info in distances:
        distance = code_info[3]
        if distance < minimum_distance:
            bad_codes.append(code_info)
    return bad_codes


def main():
    print "GENERATING CODES"
    #code_range = range(1, 50)
    code_range = range(1 << DATA_BITS)

    all_codes = generate_codes(code_range)
    print all_codes
    #'''
    print "COMPUTING MIN DISTANCES"
    distances = coding.get_min_distances(all_codes, verbose=True)
    print "SAVING DISTANCES"
    with open("min_distances.pkl", "wb") as f:
        cPickle.dump(distances, f, protocol=cPickle.HIGHEST_PROTOCOL)
    #'''
    #'''
    print "FINDING BAD CODES"
    bad_codes = get_bad_codes()
    refined_bad_codes = list(set(map(lambda c: c[0], bad_codes)))
    refined_bad_codes.sort()
    print refined_bad_codes
    #'''
    print "SAVING BAD CODES", len(refined_bad_codes)
    with open("bad_codes.pkl", "wb") as f:
        cPickle.dump(bad_codes, f, protocol=cPickle.HIGHEST_PROTOCOL)
    # Codes with bad codes removed
    code_range = list(set(code_range) - set(refined_bad_codes))
    code_range.sort()
    print "TOTAL GOOD CODES:", len(code_range)
    #'''
    #'''
    print "COMPUTING HISTOGRAM"
    hist = coding.get_histogram(all_codes)
    print "SAVING HISTOGRAM"
    with open("histogram.pkl", "wb") as f:
        cPickle.dump(hist, f, protocol=cPickle.HIGHEST_PROTOCOL)
    #'''
    visualize_histogram()

if __name__ == '__main__':
    main()


