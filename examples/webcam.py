#!/usr/bin/python
import sys

import numpy as np
import cv2

from redcomet import rcartag, util


def main(args):
    device = 0
    if len(args) > 1:
        device = int(args[1])

    rctag = rcartag.RedCometARTag()
    window_name = "RCTag Detection -- {0}".format(device)
    cv2.namedWindow(window_name)

    camera = cv2.VideoCapture()
    if not camera.open(device):
        print "Could not open camera with device index:", device
    while True:
        retval, img = camera.read()
        if retval:
            gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            results = rctag.decode(gray_img)
            rctag.draw_results(img, results)

        cv2.imshow(window_name, img)
        cv2.waitKey(10)

if __name__ == '__main__':
    try:
        main(sys.argv)
    except KeyboardInterrupt:
        pass

