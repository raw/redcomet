#!/usr/bin/python
import argparse

import cv2

from redcomet import rcartag, util


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("code", type=int)
    parser.add_argument("-d", "--dimension", type=int,
                        default=650)
    parser.add_argument("-f", "--filename", type=str,
                        default="RCTAG_%04d.png")
    args = parser.parse_args()
    rctag = rcartag.RedCometARTag()

    if args.code in rcartag.BAD_CODES:
        print "Warning: '{0}' is not a very good code.".format(args.code)
    img = rctag.encode(args.code, dimensions=(args.dimension, 
                       args.dimension))
    cv2.imwrite(args.filename % args.code, img)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass
