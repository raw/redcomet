from distutils.core import setup

setup(
    name="redcomet",
    version="0.1.0",
    author="River Allen",
    author_email="riverallen@gmail.com",
    packages=["redcomet"],
    description="A simple augmented reality tag encoder and decoder.",
    long_description=open("README.txt").read(),
    #install_requires=[
    #    "reedsolo",
    #    "numpy",
    #    "cv2",
    #    ],
)

