========
RedComet
========

What is RedComet?
=================

Redcomet is a python package for encoding and decoding a simple Augmented
Reality marker that can be printed on a piece of paper. The tag is heavily 
based on the ARTag marker developed by Mark Fiali when he was working at 
the National Research Council of Canada [#]_.


Requirements
============

 * Python 2.7
 * OpenCV 2.3+ with Python bindings (i.e. "import cv2" **MUST** work)
 * reedsolo [#]_
 * Numpy

Installation
============

  #. Download redcomet to a directory of your choosing. We'll assume it is in *<redcomet_dir>*.

  #. $ cd <redcomet_dir>

  #. $ *python setup.py install*

    * You may need to do *$ sudo python setup.py install*


Usage
=====

To generate a tag for printing, you can use one of the example scripts from the
command line:

::

  $ cd <redcomet_dir>
  $ python examples/encode.py 323

This will create an image *RCTAG_0323.png* in the current directory that can 
be printed.

With the printed tag, you can test if the tag is being seen if you have a webcam
hooked up. Using another example script from the command line:

::

  $ cd <redcomet_dir>
  $ python examples/webcam.py

If you want to use a different camera then whatever may be the default (usually
the builtin laptop webcam):
::

  $ python examples/webcam.py 1

Where 1, 2, 3, ..., etc. will be the camera's device number.

Example code for decoding an image:

  >>> import cv2
  >>> from redcomet import RedCometARTag
  >>> rctag = RedCometARTag()
  >>> img = cv2.imread("input.png", cv2.CV_LOAD_IMAGE_GRAYSCALE)
  >>> results = rctag.decode(img)

**cv2** is only used to load an image in this example. RedCometARTag.decode() only
requires that the image is a 2D numpy array that is grayscale (single channel).

Known Issues
============

 * Obstruction of the marker is a bane to the algorithm. Simply put: *avoid obstructing the marker*.
 * Avoid these tag codes and ignore them if they are detected:

   * 0, 1, 16, 17, 50, 99, 134, 157, 226, 227, 242, 243, 262, 263, 278, 279, 280, 385, 484, 485, 500, 501, 560, 576, 588, 616, 638, 680, 681, 696, 697, 710, 716, 717, 732, 733, 798, 808, 899, 921, 942, 943, 958, 959, 970, 971, 986, 987, 1026, 1027, 1042, 1043, 1102, 1103, 1108, 1118, 1119, 1163, 1186, 1218, 1219, 1234, 1235, 1254, 1255, 1256, 1257, 1270, 1271, 1272, 1273, 1284, 1285, 1300, 1301, 1352, 1353, 1362, 1368, 1369, 1379, 1432, 1476, 1477, 1492, 1493, 1504, 1505, 1518, 1519, 1520, 1521, 1534, 1535, 1572, 1573, 1588, 1589, 1642, 1643, 1658, 1659, 1702, 1723, 1732, 1733, 1748, 1749, 1796, 1826, 1827, 1828, 1842, 1843, 1850, 1857, 1881, 1900, 1901, 1916, 1917, 1980, 1986, 1987, 2002, 2003, 2178, 2179, 2194, 2195, 2264, 2301, 2417, 2418, 2436, 2437, 2452, 2453, 2638, 2639, 2654, 2655, 2660, 2661, 2676, 2677, 2701, 2711, 2722, 2723, 2738, 2739, 2842, 2888, 2889, 2904, 2905, 2914, 2915, 2930, 2931, 2957, 2965, 2980, 2981, 2996, 2997, 3114, 3115, 3130, 3131, 3173, 3183, 3231, 3272, 3273, 3288, 3289, 3372, 3373, 3388, 3389, 3497, 3516, 3530, 3534, 3535, 3550, 3551, 3587, 3649, 3669, 3680, 3681, 3696, 3697, 3732, 3750, 3751, 3766, 3767, 3814, 3815, 3830, 3831, 3852, 3942, 3943, 3958, 3959, 3985, 4000, 4001, 4016, 4017, 4052, 4064, 4065, 4080, 4081

Notes
=====

.. [#] http://www.cs.cmu.edu/afs/cs/project/skinnerbots/Wiki/AprilTags/NRC-47419.pdf
.. [#] https://pypi.python.org/pypi/reedsolo
