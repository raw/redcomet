import unittest

import numpy as np

from redcomet import coding


class BCHTest(unittest.TestCase):

    def setUp(self):
        pass

    def test_divide_00011(self):
        self.assertEquals(coding.bch_divide(0b000111101011001, 
                                            0b10100110111, 5, 10), 
                          0)

    def test_encode_00011(self):
        self.assertEquals(coding.bch_encode(0b00011, 0b10100110111, 5, 10),
                          0b000111101011001)


class HammingTest(unittest.TestCase):

    def test_hamming_weight(self):
        self.assertEquals(coding.hamming_weight(0b1110111), 6)

    def test_hamming_distance(self):
        self.assertEquals(coding.hamming_dist(0b1010101, 0b1101011), 5)

    def test_code_distances(self):
        codes = [32, 0, 7, 1 << 32]
        distances = coding.get_distances(codes)
        expected = {32: {0:1, 7:4, (1 << 32):2},
                    0: {7:3, (1<<32):1},
                    7: {(1<<32):4}}
        self.assertDictEqual(distances, expected)

    def test_code_hist(self):
        # codes [[data, rotation, encoding], ...]
        # Testing hamming, so data and rotation are irrelevant.
        codes = [[0, -1, 32], [1, -1, 0], [2, -1, 7], [3, -1, 1 << 32]]
        hist = coding.get_histogram(codes)
        expected = {1:2, 2:1, 3:1, 4:2}
        self.assertDictEqual(hist, expected)

    def test_min_code_distances(self):
        # codes [[data, rotation, encoding], ...]
        # Testing hamming, so data and rotation are irrelevant.
        codes = [[0, -1, 32], [1, -1, 0], [2, -1, 7], [3, -1, 1 << 32]]
        min_distances = coding.get_min_distances(codes)
        # min_distances [code, rotation, message, min dist, min dist count]
        expected = np.int64([[0, -1, 32, 1, 1],
                             [1, -1, 0, 1, 2],
                             [2, -1, 7, 3, 1],
                             [3, -1, 1<<32, 1, 1]])
        self.assertTrue(np.all(min_distances == expected))


def main():
    unittest.main()

if __name__ == '__main__':
    main()
