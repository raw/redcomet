import os
import unittest

import numpy as np
import cv2

from redcomet import rcartag

IMG_PATH = os.path.join(os.path.abspath("images"), "{0}")


class RCTagTest(unittest.TestCase):

    def setUp(self):
        self.rctag = rcartag.RedCometARTag()

    def tearDown(self):
        self.rctag = None

    def test_convert_data_to_byte_greater(self):
        self.assertRaises(ValueError, self.rctag.convert_data_to_bytearray,
                          (1 << 12)+1)

    def test_convert_data_to_byte(self):
        result = self.rctag.convert_data_to_bytearray(1025)
        self.assertEquals(result, bytearray(['\x04', '\x01']))

    def test_convert_byte_to_data(self):
        byte_input = self.rctag._rs_codec.encode([4,1])
        result = self.rctag.convert_bytearray_to_data(byte_input)
        expected = 1025
        self.assertEquals(result, expected)

    def test_convert_byte_to_mat(self):
        input_data = bytearray([12, 8, 4, 2, 1])
        result = self.rctag.convert_bytearray_to_mat(input_data)
        expected = np.array([[1, 1, 0, 0, 0, 0],
                             [0, 0, 1, 0, 0, 0],
                             [0, 0, 0, 0, 0, 1],
                             [0, 0, 0, 0, 0, 0],
                             [0, 0, 1, 0, 0, 0],
                             [0, 0, 0, 0, 0, 1]])
        error_msg = '''input_data: %r
Result:
%s''' % (input_data, str(result))
        self.assertTrue(np.all(result == expected), msg=error_msg)

    def test_convert_mat_to_byte(self):
        input_mat = np.array([[1, 1, 0, 0, 0, 0],
                              [0, 0, 1, 0, 0, 0],
                              [0, 0, 0, 0, 0, 1],
                              [0, 0, 0, 0, 0, 0],
                              [0, 0, 1, 0, 0, 0],
                              [0, 0, 0, 0, 0, 1]])
        expected = bytearray([12, 8, 4, 2, 1])
        result = self.rctag.convert_mat_to_bytearray(input_mat)
        self.assertEquals(result, expected)


    def test_rotate_cw(self):
        input_mat = np.array([[1, 1, 0, 0, 0, 0],
                              [0, 0, 1, 0, 0, 0],
                              [0, 0, 0, 0, 0, 1],
                              [0, 0, 0, 0, 0, 0],
                              [0, 0, 1, 0, 0, 0],
                              [0, 0, 0, 0, 0, 1]])

        expected_mat = np.array([[0, 0, 0, 0, 0, 1],
                                 [0, 0, 0, 0, 0, 1],
                                 [0, 1, 0, 0, 1, 0],
                                 [0, 0, 0, 0, 0, 0],
                                 [0, 0, 0, 0, 0, 0],
                                 [1, 0, 0, 1, 0, 0]])

        result = self.rctag.rotate_mat_cw(input_mat)
        error_msg = '''input_mat: 
{0}
Result:
{1}'''.format(input_mat, result)
        self.assertTrue(np.all(result == expected_mat), msg=error_msg)

    def test_flip(self):
        input_mat = np.array([[1, 1, 0, 0, 0, 0],
                              [0, 0, 1, 0, 0, 0],
                              [0, 0, 0, 0, 0, 1],
                              [0, 0, 0, 0, 0, 0],
                              [0, 0, 1, 0, 0, 0],
                              [0, 0, 0, 0, 0, 1]])

        expected_mat = np.array([[0, 0, 0, 0, 1, 1],
                                 [0, 0, 0, 1, 0, 0],
                                 [1, 0, 0, 0, 0, 0],
                                 [0, 0, 0, 0, 0, 0],
                                 [0, 0, 0, 1, 0, 0],
                                 [1, 0, 0, 0, 0, 0]])

        result = self.rctag.flip_mat(input_mat)
        error_msg = '''input_mat: 
{0}
Result:
{1}'''.format(input_mat, result)
        self.assertTrue(np.all(result == expected_mat), msg=error_msg)

    def test_rctag_detection(self):
        img = cv2.imread(IMG_PATH.format("detection.png"), 
                         cv2.CV_LOAD_IMAGE_GRAYSCALE)
        result = self.rctag.decode(img)
        expected_pos1 = np.array([[626,373], [892,373], [634,648], [915,641]])
        expected_pos2 = np.array([[341,52], [613,66], [341,325], [618,327]])
        # Check code values are correct
        self.assertEquals(result[0][1], 6)
        self.assertEquals(result[1][1], 3)

    def test_rctag_no_detection_invalid_code(self):
        img = cv2.imread(IMG_PATH.format("no_detection_invalid_code.png"),
                         cv2.CV_LOAD_IMAGE_GRAYSCALE)
        result = self.rctag.decode(img)
        expected = []
        self.assertEquals(result, expected)

    def test_rctag_no_detection_wrong_type(self):
        img = cv2.imread(IMG_PATH.format("no_detection_wrong_type.png"),
                         cv2.CV_LOAD_IMAGE_GRAYSCALE)
        result = self.rctag.decode(img)
        expected = []
        self.assertEquals(result, expected)

    def test_rctag_perspective_boundary1(self):
        # Boundary Image 1
        img = cv2.imread(IMG_PATH.format("perspective_boundary1.png"), 
                         cv2.CV_LOAD_IMAGE_GRAYSCALE)
        result = self.rctag.decode(img)
        expected_pos = np.array([[537,324], [89,325], [593,126], [223,86]])
        # Only one tag is detected (code 3)
        self.assertEquals(result, [])

    def test_rctag_perspective_boundary2(self):
        # Boundary Image 2
        img = cv2.imread(IMG_PATH.format("perspective_boundary2.png"), 
                         cv2.CV_LOAD_IMAGE_GRAYSCALE)
        result = self.rctag.decode(img)
        self.assertEquals(result, [])

    def test_rctag_perspective_boundary3(self):
        # Boundary Image 3
        # This image finds a really bizzare rectangle within the clutter
        # of the desk.
        img = cv2.imread(IMG_PATH.format("perspective_boundary3.png"), 
                         cv2.CV_LOAD_IMAGE_GRAYSCALE)
        result = self.rctag.decode(img)
        self.assertEquals(result, [])

    def test_rctag_perspective_boundary4(self):
        # Boundary Image 4
        # Similar to boundary image 3
        img = cv2.imread(IMG_PATH.format("perspective_boundary4.png"), 
                         cv2.CV_LOAD_IMAGE_GRAYSCALE)
        result = self.rctag.decode(img)
        self.assertEquals(result, [])

    def test_rctag_threshold_basic(self):
        # Normal conditions.
        img = cv2.imread(IMG_PATH.format("thresh_evaluation_bright.png"),
                         cv2.CV_LOAD_IMAGE_GRAYSCALE)
        result = self.rctag.decode(img)
        self.assertTrue(len(result) > 0)
        self.assertEquals(result[0][1], 72)

    # Each image gets progressively darker.
    def test_rctag_threshold_dark1(self):
        img = cv2.imread(IMG_PATH.format("thresh_evaluation_dark1.png"),
                         cv2.CV_LOAD_IMAGE_GRAYSCALE)
        result = self.rctag.decode(img)
        self.assertTrue(len(result) > 0)
        self.assertEquals(result[0][1], 72)

    """
    def test_rctag_threshold_dark2(self):
        # TODO: Note working
        img = cv2.imread(IMG_PATH.format("thresh_evaluation_dark2.png"),
                         cv2.CV_LOAD_IMAGE_GRAYSCALE)
        result = self.rctag.decode(img, threshold_c=-1, 
                                   threshold_window_percent=0.1)
        self.assertTrue(len(result) > 0)
        self.assertEquals(result[0][1], 72)

    def test_rctag_threshold_dark3(self):
        # TODO: Note working
        img = cv2.imread(IMG_PATH.format("thresh_evaluation_dark3.png"),
                         cv2.CV_LOAD_IMAGE_GRAYSCALE)
        result = self.rctag.decode(img, threshold_c=21, 
                                   threshold_window_percent=0.9)
        self.assertTrue(len(result) > 0)
        self.assertEquals(result[0][1], 72)
    """

    def test_rctag_misread1(self):
        # Misread Test 1
        img = cv2.imread(IMG_PATH.format("misread1.png"),
                         cv2.CV_LOAD_IMAGE_GRAYSCALE)
        result = self.rctag.decode(img)
        self.assertEquals(result[0][1], 7)
        self.assertEquals(result[1][1], 9)

    def test_rctag_misread2(self):
        # Misread Test 2
        img = cv2.imread(IMG_PATH.format("misread2.png"),
                         cv2.CV_LOAD_IMAGE_GRAYSCALE)
        result = self.rctag.decode(img)
        self.assertEquals(result[0][1], 7)

    def test_rctag_misread_with_border_check(self):
        img = cv2.imread(IMG_PATH.format("misread_with_border_check.png"),
                         cv2.CV_LOAD_IMAGE_GRAYSCALE)
        result = self.rctag.decode(img)
        self.assertEquals(result, [])

    def test_rctag_misread_environment1(self):
        # Misread red block
        # TODO: Not working
        img = cv2.imread(IMG_PATH.format("misread_environment1.png"),
                         cv2.CV_LOAD_IMAGE_GRAYSCALE)
        result = self.rctag.decode(img)
        self.assertEquals(result, [])

    """
    def test_rctag_misread_environment2(self):
        # Misread picture of roomba
        img = cv2.imread(IMG_PATH.format("misread_environment2.png"),
                         cv2.CV_LOAD_IMAGE_GRAYSCALE)
        result = self.rctag.decode(img, threshold_c=10)
        self.assertEquals(result, [])
    #TODO:
    def test_rctag_occlusion(self):
        img = cv2.imread(IMG_PATH.format("occlusion.png', 
                         cv2.CV_LOAD_IMAGE_GRAYSCALE)
        result = self.rctag.decode(img)#, debug=True)
        self.assertEquals(0, 1)
    """


def main():
    unittest.main()

if __name__ == '__main__':
    main()
