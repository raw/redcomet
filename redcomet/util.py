import numpy as np

PI = np.pi
norm = np.linalg.norm

def dist(a, b):
    # http://stackoverflow.com/questions/1401712/calculate-euclidean-distance-with-numpy
    return norm(a-b)

def angle(p1, p2):
    return np.arctan2(p2[1]-p1[1], p2[0]-p1[0])

def get_random_color():
    return tuple(np.random.randint(32, 255, 3))

def min_diff_angle(x, y):
    """This function will find the minimum difference between two angles."""
    z = abs(x - y)
    if z <= PI:
        return z
    else: 
        flip_diff = z - PI
        return PI - flip_diff
