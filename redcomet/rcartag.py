import itertools
import numpy as np
import cv2

import reedsolo

import util


PI = np.pi
PI2 = 2.0 * np.pi

BAD_CODES = [0, 1, 16, 17, 50, 99, 134, 157, 226, 227, 242, 243, 262, 263, 278, 279, 280, 385, 484, 485, 500, 501, 560, 576, 588, 616, 638, 680, 681, 696, 697, 710, 716, 717, 732, 733, 798, 808, 899, 921, 942, 943, 958, 959, 970, 971, 986, 987, 1026, 1027, 1042, 1043, 1102, 1103, 1108, 1118, 1119, 1163, 1186, 1218, 1219, 1234, 1235, 1254, 1255, 1256, 1257, 1270, 1271, 1272, 1273, 1284, 1285, 1300, 1301, 1352, 1353, 1362, 1368, 1369, 1379, 1432, 1476, 1477, 1492, 1493, 1504, 1505, 1518, 1519, 1520, 1521, 1534, 1535, 1572, 1573, 1588, 1589, 1642, 1643, 1658, 1659, 1702, 1723, 1732, 1733, 1748, 1749, 1796, 1826, 1827, 1828, 1842, 1843, 1850, 1857, 1881, 1900, 1901, 1916, 1917, 1980, 1986, 1987, 2002, 2003, 2178, 2179, 2194, 2195, 2264, 2301, 2417, 2418, 2436, 2437, 2452, 2453, 2638, 2639, 2654, 2655, 2660, 2661, 2676, 2677, 2701, 2711, 2722, 2723, 2738, 2739, 2842, 2888, 2889, 2904, 2905, 2914, 2915, 2930, 2931, 2957, 2965, 2980, 2981, 2996, 2997, 3114, 3115, 3130, 3131, 3173, 3183, 3231, 3272, 3273, 3288, 3289, 3372, 3373, 3388, 3389, 3497, 3516, 3530, 3534, 3535, 3550, 3551, 3587, 3649, 3669, 3680, 3681, 3696, 3697, 3732, 3750, 3751, 3766, 3767, 3814, 3815, 3830, 3831, 3852, 3942, 3943, 3958, 3959, 3985, 4000, 4001, 4016, 4017, 4052, 4064, 4065, 4080, 4081]


class RedCometARTag(object):
    """
The RedComet Augmented Reality tag object. This object can generate tags for
printing out (see encode()) as well as attempt to decode the presence of 
tags in an image (see decode()).
    """

    def __init__(self, samples_per_square=1):
        """
Create the RedCometARTag object. samples_per_square is used in the decoding 
process. By default, it is set to 1 -- that is it only uses 1 sample point to
determine a bit's value. If samples_per_square is set to 2 -- it will use 4
sample points. If set to 3, it will use 9 sample points and so on. Using more
sample points will increase decoding time and will not necessarily lead to
better results.
        """
        # The data and error correcting codes are encoded in a 6x6 bit matrix.
        self._interior = 6
        # The border thickness is 2 on all sides.
        self._border = 2
        # Of the 36 bits (6x6), 12 are for data.
        self._data_bits = 12
        # The remaining 24 bits are for the Reed-Solomon error correcting code.
        self._rs_bits = 24
        self._rs_bytes = 3
        # We'll use reedsolo's codec to encode the error correcting code.
        self._rs_codec = reedsolo.RSCodec(self._rs_bytes)
        # This will allow us to perform rotations on the 6x6 bit matrix.
        self._flip_matrix = np.array([[0, 0, 0, 0, 0, 1],
                                      [0, 0, 0, 0, 1, 0],
                                      [0, 0, 0, 1, 0, 0],
                                      [0, 0, 1, 0, 0, 0],
                                      [0, 1, 0, 0, 0, 0],
                                      [1, 0, 0, 0, 0 ,0]])

        # We create a perspective matrix that maps from
        # (0, 20) -------- (20, 20)
        #    |                 |
        #    |                 |
        #    |                 |
        # (0, 0)  -------- (20, 0)
        #
        # to each of the 4 corners of our detected square.
        # Hence, the odd numbers are midpoints of squares in our grid.
        # The data points go from 2:8 in x and y in the marker which map to
        # 5:15 in x and y. That might seem like enough, but to reduce false
        # positives it also worthwhile to read the border to make sure it
        # is all black.
        self._samples_per_square = samples_per_square
        #self.encoding_samples_size = (6 * 6) * self.samples_per_square
        #self.all_samples_size = 
        grid_length = 20 * self._samples_per_square
        self._sample_rect = np.float32([[0, 0], [0, grid_length],
                                        [grid_length, 0], 
                                        [grid_length, grid_length]])

        # Our marker is symmetric so columns are the same size as rows.
        full_grid_row = np.linspace(1, 
                                    grid_length - 1, 
                                    ((2 * self._border) + self._interior) 
                                      * self._samples_per_square)
        first_interior_point = 1 + (2 * self._border * self._samples_per_square) 
        data_grid_row = np.linspace(first_interior_point,
                                    grid_length - 1 - (2 * self._border * self._samples_per_square), 
                                    self._interior * self._samples_per_square)
        full_sample_points = []
        for element in itertools.product(full_grid_row, full_grid_row):
            full_sample_points.append(element)
        data_sample_points = []
        for element in itertools.product(data_grid_row, data_grid_row):
            data_sample_points.append(element)
        # The border is the difference of all sample points and data points.
        border_points = set(full_sample_points) - set(data_sample_points)
        # (The extra np.array([]) is to get around cv expecting
        # 1-3 channels -- RGB. The [0] is the same logic.)
        self._border_sample_points = np.array([list(border_points)], np.float32)
        self._data_sample_points = np.array([data_sample_points], np.float32)

    def rs_encode(self, arg):
        """Encode the Reed-Solomon code given a number arg."""
        data_bytes = self.convert_data_to_bytearray(arg)
        return self._rs_codec.encode(data_bytes)

    def rs_decode(self, byte_readings):
        """Decode a value with a Reed-Solomon encoding of bytes."""
        return self._rs_codec.decode(byte_readings)

    def convert_bytearray_to_data(self, arg):
        """Takes a bytearray (used in the RS encoding) and returns data number."""
        return (arg[0] << 8) + (arg[1] & 255)

    def convert_data_to_bytearray(self, arg):
        """Take a number and return it as a byte array."""
        if arg < 0 or arg > (1 << self._data_bits):
            raise ValueError("Argument must be [0-{0}] inclusive: {1}".format(
                             (1 << self._data_bits), arg))
        return bytearray([chr(arg >> 8), chr(arg & 255)])

    def convert_bytearray_to_mat(self, arg):
        """Take a bytearray and convert it to a binary 6x6 matrix."""
        dim = (6, 6)
        k = dim[0] * dim[1]
        mat = np.zeros(k, np.int)
        # We start at the right most bit in the last byte and move
        # left. These operations are confusing, but they allow us to
        # produce a big endian (most significant bit first) output where
        # given a bytearray:
        #
        # convert_bytearray_to_mat( bytearray([12,8,4,2,1]) )
        #
        # Returns (NOTE the first/leftmost byte only uses 4 bits):
        #
        # [[1, 1, 0, 0, 0, 0],
        #  [0, 0, 1, 0, 0, 0],
        #  [0, 0, 0, 0, 0, 1],
        #  [0, 0, 0, 0, 0, 0],
        #  [0, 0, 1, 0, 0, 0],
        #  [0, 0, 0, 0, 0, 1]]
        for i in xrange(k - 1,-1,-1):
            arg_index = (i + 4) / 8
            bit_index = ((k - 1) - i) % 8
            mat[i] = (arg[arg_index] >> bit_index) & 1
        mat = np.reshape(mat, dim)
        return mat

    def convert_mat_to_bytearray(self, mat):
        """Take a 6x6 matrix and convert it to a byte array used by reedsolo."""
        data = np.ravel(mat)
        byte_buffer = bytearray([0] * 5)
        k = len(data)
        # We follow the same logic as convert_bytearray_to_mat
        # but in the opposite direction. See that function above
        # for more details.
        for i in xrange(k - 1,-1,-1):
            arg_index = (i + 4) / 8
            bit_index = ((k - 1) - i) % 8
            byte_buffer[arg_index] ^= data[i] << bit_index
        return byte_buffer

    def encode(self, arg, dimensions=None):
        """
Given a valid number argument (1 to 4095), this function will generate convert
the number into a RedCometTag encoding. The result is a numpy matrix that
can then be saved and printed. To conveniently just save the file, use the
function encode_and_save(). The dimensions of the matrix (and size of the 
image) are 600x600 pixels by default and can be altered using the 
dimensions parameter. The x and y dimensions should be the 
same (i.e. a square image); otherwise, dimensions = [max(x, y), max(x, y)].

>>> from redcomet import RedCometARTag
>>> rctag = RedCometARTag()
>>> tag_img = rctag.encode(323)
        """
        data = self.rs_encode(arg)
        return self._generate(data, dimensions)

    def encode_and_save(self, arg, filename, dimensions=None):
        """
Encodes a valid number argument (1 to 4095) into a RedCometTag and saves it
to filename. See encode() for more details.

>>> from redcomet import RedCometARTag
>>> rctag = RedCometARTag()
>>> code = 323
>>> rctag.encode_and_save(code, "RCTAG_%03d.png" % code)
        """
        data = self.rs_encode(arg)
        tag_img = self._generate(data, dimensions)
        cv2.imwrite(filename, tag_img)

    def _generate(self, data, dimensions=None):
        if dimensions is None: 
            dimensions = (600,600)
        if dimensions[0] != dimensions[1]:
            # If not square...make it square.
            dim = max(*dimensions)
            dimensions = (dim, dim)
        img = np.zeros(dimensions)
        # RC ARTag has 10x10 bit blocks. 6x6 interior and 2-width border
        bit_block_size = dimensions[0] / (2 * self._border + self._interior)

        # Generate the white blocks for the image
        img = np.zeros(dimensions)
        mat_dim = (self._interior, self._interior)
        data_mat = self.convert_bytearray_to_mat(data)
        for i in xrange(self._interior):
            for j in xrange(self._interior):
                p_x = (i + self._border) * bit_block_size
                p_y = (j + self._border) * bit_block_size
                if data_mat[i,j]:
                    img[p_x:p_x + bit_block_size, 
                        p_y:p_y + bit_block_size] = 255
        return img

    def decode(self, img, threshold_c=-1, sigma=0.4,
               arcPercent=0.1, threshold_window_percent=0.3, 
               debug=False, **kwargs):
        """
Given an image with or without the presence of RedComet tags, this function
will attempt to detect them and report on their status. NOTE: img MUST BE
A GRAYSCALE numpy array. For example:

>>> from redcomet import RedCometARTag
>>> input_filename = "input.png"
>>> input_img = cv2.imread(input_filename, cv2.CV_LOAD_IMAGE_GRAYSCALE)
>>> rctag = RedCometARTag()
>>> results = rctag.decode(input_img)
>>> print "Detections:", results

Regarding parameters, threshold_c is the C value used for auto thresholding the
image. threshold_window_percent will determine the size of the window for
auto thresholding. The sigma parameter determines the amount of blurring used
to reduce noise in the image. arcPercent should be left alone. If debug=True, 
then the function will save a plethora of images to the current directory 
(VERY SLOW). This is purely for debugging purposes and should not be used 
otherwise."""
        possible_matches = self.find_rectangles(img, **kwargs)

        # Keep a blurred image for finding corners.
        smooth = cv2.GaussianBlur(img, (5,5), sigma)
        # Keep a thresholded image for finding 
        thresh = self.get_thresholded_image(smooth, threshold_c,
                                            threshold_window_percent)

        # We now have a list of rectangle candidates. We will attempt to
        # decode each rectangle. If it is valid we will append it to results.
        results = []
        # These two variables are for debugging purposes.
        rect_debug_img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        rect_number = 0
        for rect_contour in possible_matches:
            # Refine corners to subpixel accuracy.
            corners = np.array(rect_contour[:], dtype=np.float32)
            cv2.cornerSubPix(smooth, corners, (3,3), (-1,-1), 
                             (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_MAX_ITER,
                              200, 0.01))

            # Pick random origin corner (corner1) and other corners
            corner1 = corners[0][0][:]
            corner2 = corners[1][0][:]
            corner3 = corners[2][0][:]
            corner4 = corners[3][0][:]

            # We need to find the opposite corner to corner1. The neighbour
            # corners can be found by finding the angles with the maximum
            # separation. The leftover corner MUST be the opposite corner.
            angle2 = util.angle(corner1, corner2) % PI2
            angle3 = util.angle(corner1, corner3) % PI2
            angle4 = util.angle(corner1, corner4) % PI2

            diff_angle23 = util.min_diff_angle(angle2, angle3)
            diff_angle24 = util.min_diff_angle(angle2, angle4)
            diff_angle34 = util.min_diff_angle(angle3, angle4)

            max_i = np.argmax([diff_angle23, diff_angle24, diff_angle34])

            # If max_i == 0 then our corners are fine (corner4 is opposite)
            # If max_i == 1 then corner3 is the true opposite
            # If max_i == 2 then corner2 is the true opposite
            if max_i == 1:
                temp = corner3.copy()
                corner3 = corner4
                corner4 = temp
            elif max_i == 2:
                temp = corner2.copy()
                corner2 = corner4
                corner4 = temp

            # Needed for transform 
            persp_rect = np.float32([corner1, corner2, corner3, corner4])
            persp_mat = cv2.getPerspectiveTransform(self._sample_rect,
                                                    persp_rect)

            # Before reading data points, let's sample border points and
            # make sure enough of them are below threshold. This will help
            # reduce false positives.
            border_points = np.int32(cv2.perspectiveTransform(
                                     self._border_sample_points, persp_mat))[0]

            # Transform the sample points to pixel perspective points
            persp_points = np.int32(cv2.perspectiveTransform(
                                    self._data_sample_points, persp_mat))[0]

            # DEBUG POINTS
            if debug:
                debug_sample_img = cv2.cvtColor(thresh, cv2.COLOR_GRAY2BGR)
                # Draw data sample points
                for xy in persp_points:
                    cv2.circle(debug_sample_img, (xy[0],xy[1]), 0, (0,150,250), -1)
                # Draw border sample points
                for i in xrange(len(border_points)):
                    xy = (border_points[i,0], border_points[i,1])
                    cv2.circle(debug_sample_img, xy, 0, (0,250,150), -1)
                # DEBUG CORNERS
                cv2.line(debug_sample_img, (int(corner1[0]), int(corner1[1])),
                         (int(corner2[0]), int(corner2[1])), (0, 0, 200), 1)
                cv2.line(debug_sample_img, (int(corner1[0]), int(corner1[1])),
                         (int(corner3[0]), int(corner3[1])), (0, 0, 200), 1)
                cv2.line(debug_sample_img, (int(corner2[0]), int(corner2[1])),
                         (int(corner4[0]), int(corner4[1])), (0, 0, 200), 1)
                cv2.line(debug_sample_img, (int(corner3[0]), int(corner3[1])),
                         (int(corner4[0]), int(corner4[1])), (0, 0, 200), 1)

                cv2.imwrite("samples_%03d.png" % rect_number, debug_sample_img)


            # There are cases where very peculiar rectangles will be formed
            # and the code will attempt to sample points way out there.
            # This should catch them. The problem with this is it may catch
            # legitimate bugs that should be corrected grabbing tags that
            # are on a weird angle.
            try:
                border_readings = thresh[border_points[:,1], border_points[:,0]]
            except IndexError as e:
                continue

            # Grad all border bits that have been read as white (1).
            border_readings = border_readings > 0
            # Count the number of border readings that are 'white' if
            # more than 5 percent of the readings are 'white' then it's
            # probably not a valid marker.
            border_percent = (np.sum(border_readings) / 
                              float(border_readings.shape[0]))
            if border_percent > 0.05:
                continue

            # Read the pixel values from autothresholded image and put them in
            # a convenient 6x6 matrix.
            try:
                mat_readings = thresh[persp_points[:,1], persp_points[:,0]]
            except IndexError as e:
                # There are cases where very peculiar rectangles will be formed
                # and the code will attempt to sample points way outside
                # the image. A few test cases have been made to test this.
                # This should catch them. The problem with this is it may catch
                # legitimate bugs that should be corrected to allow tags that
                # are on a weird angle.
                # For now, it works well enough.
                continue
            # Using sample points, we now grab pixel values from the image
            # to begin the process of building RedComet tag 36 bit string.
            # The code here gets a bit complicated as it needs to deal with
            # the case of using multiple sample points on the image to 
            # determine a single bit in the 36 bit string.
            m_height = m_width = np.sqrt(len(mat_readings))
            mat_readings = mat_readings.reshape(m_width, m_height)
            mat_readings = mat_readings / 255.0
            samples_in_sq = self._samples_per_square
            samples_in_sq2 = samples_in_sq * samples_in_sq
            total_sample_points = samples_in_sq2
            total_bits = self._data_bits + self._rs_bits
            new_mat_readings = np.zeros(total_bits)
            for i in xrange(total_bits):
                x = ((i % 6) * samples_in_sq)
                y = (int(i / 6) * samples_in_sq)
                sample_reading = np.sum(np.sum(mat_readings[x:x+samples_in_sq,
                                                              y:y+samples_in_sq]
                                        ))
                sample_reading = sample_reading / samples_in_sq2
                new_mat_readings[i] = sample_reading

            mat_readings = new_mat_readings.reshape((6,6))
            mat_readings = mat_readings >= 0.5

            # Convert the 6x6 matrix to byte array for reed.
            byte_readings = self.convert_mat_to_bytearray(mat_readings)
            if debug:
                generated_img = self.generate(byte_readings, (450, 450))
                cv2.imwrite("data_%03d.png" % rect_number, generated_img)

            # We are going to rotate the matrix 3 times to check
            # all orientations (4). If success_flag comes out true then it
            # means we detected one.
            success_flag = False
            for i in xrange(4):
                try:
                    data_bytes = self.rs_decode(byte_readings)
                    success_flag = True
                    break
                except reedsolo.ReedSolomonError:
                    # Was not able to decode.
                    pass
                # Rotate data.
                mat_readings = self.rotate_mat_cw(mat_readings)
                byte_readings = self.convert_mat_to_bytearray(mat_readings)

            # We can use 'i' to determine the rotation we were able to decode.
            if success_flag:
                data_value = self.convert_bytearray_to_data(byte_readings)
                # data_value of 0 is all black...this is false positive
                if data_value == 0:
                    continue
                if i == 0:
                    # corner1 is bottom left
                    results.append([np.array([corner1, corner2, 
                                    corner3, corner4]), data_value])
                elif i == 1:
                    # One ccw rotation, corner2 is bottom left
                    results.append([np.array([corner2, corner4, 
                                    corner1, corner3]), data_value])
                elif i == 2:
                    # Two ccw rotations, corner4 is bottom left
                    results.append([np.array([corner4, corner3, 
                                    corner2, corner1]), data_value])
                elif i == 3:
                    # Three ccw rotations, corner3 is bottom left
                    results.append([np.array([corner3, corner1, 
                                    corner4, corner2]), data_value])

            # Parse through the next detected rectangle.
            rect_number += 1

        if debug:
            debug_result_img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
            self.draw_results(debug_result_img, results)
            cv2.imwrite("results.png", debug_result_img)
        return results


    def find_rectangles(self, img, threshold_c=1, sigma=0.4,
                        arcPercent=0.1, threshold_window_percent=0.3, 
                        debug=False, **kwargs):
        """
Detects possible rectangles to find the marker. Used by decode().
        """
        # To increase the effectiveness of RCTag detection, the image is
        # downsampled several times to better detect rectangles. Some rectangle
        # may have minor imperfections in the original resolution that 
        # will become negligble in a smaller resolution.
        downsample_times = 3
        contours = []
        scale = 1.
        for i in xrange(downsample_times):
            scale = np.power(2., i)
            scaled_img = cv2.resize(img, (0,0), fx=1./scale, fy=1./scale) 
            smooth = cv2.GaussianBlur(scaled_img, (5,5), sigma)
            thresh = self.get_thresholded_image(smooth, threshold_c,
                                                threshold_window_percent)
            edges = cv2.Canny(thresh, 100, 170)
            s_contours, heir = cv2.findContours(edges, cv2.RETR_LIST,
                                                cv2.CHAIN_APPROX_SIMPLE)
            contours.extend(s_contours * (1./scale))

        # Group edges to find rectangles.
        possible_matches = []
        for contour in contours:
            # http://stackoverflow.com/questions/8667818/opencv-c-obj-c-detecting-a-sheet-of-paper-square-detection/8863060#8863060
            arc = cv2.arcLength(contour, True)
            approx_poly = cv2.approxPolyDP(contour, arc * arcPercent, True)

            # Detect Rectangles
            if (approx_poly.shape[0] == 4 and
                cv2.contourArea(approx_poly) > 200):
                possible_matches.append(approx_poly)
        return possible_matches

    def get_thresholded_image(self, smooth, threshold_c=-1, 
                              threshold_window_percent=0.3):
        """
This function will convert a grayscale to either black or white. The choice
of either black or white is done using a local window as described in the
opencv documentation for adaptiveThreshold() with the 
ADAPTIVE_THRESH_MEAN_C. The smooth parameter expects an image that has undergone
some smoothing for noise reduction. The threshold_window_percent parameter will 
create a thresholding window size based on the size of the image.
        """
        threshold_window_size = int(min(smooth.shape) * threshold_window_percent)
        if threshold_window_size % 2 == 0:
            threshold_window_size += 1
        thresh = cv2.adaptiveThreshold(smooth, 255, cv2.ADAPTIVE_THRESH_MEAN_C,
                                       cv2.THRESH_BINARY,
                                       threshold_window_size, threshold_c)
        return thresh

    def draw_results(self, img, results, rectangle_color=None, 
                     text_color=None, rectangle_thickness=1):
        """
This is a help visualization function that  will draw a crude rectangle w/ 
marker number over where the marker(s) were detected.

The img parameter should be the original image, as a numpy array. This function
does not require that the image be black and white. The results parameter is
the return value from 

>>> from redcomet import RedCometARTag
>>> fname = "input.png"
>>> bw_img = cv2.imread(fname, cv2.CV_LOAD_IMAGE_GRAYSCALE)
>>> rctag = RedCometARTag()
>>> results = rctag.decode(bw_img)
>>> visualization_img = cv2.imread(fname)
>>> rctag.draw_results(visualization_img, results)
>>> # Save the visualization image.
>>> cv2.imwrite("output.png", visualization_img)
        """
        if rectangle_color is None:
            rectangle_color = (45, 45, 255)
        if text_color is None:
            text_color = (45, 150, 220)

        for result in results:
            code = result[1]
            corners = result[0]
            midpoint_dist = util.dist(corners[0], corners[3]) / 2.
            t_corners = map(lambda x: tuple(x), corners)
            # Draw the corners of the marker
            cv2.line(img, t_corners[0], t_corners[1], rectangle_color, 
                     rectangle_thickness)
            cv2.line(img, t_corners[0], t_corners[2], rectangle_color, 
                     rectangle_thickness)
            cv2.line(img, t_corners[1], t_corners[3], rectangle_color,
                     rectangle_thickness)
            cv2.line(img, t_corners[2], t_corners[3], rectangle_color,
                     rectangle_thickness)
            # Draw the marker code
            # Draw a background over the marker so the code can be seen.
            cv2.fillConvexPoly(img, np.int32([corners[0], corners[1], 
                               corners[3], corners[2]]), (0,0,0))
            # Find the angle between corner3 and corner1
            midpoint_angle = np.arctan2(corners[3][1] - corners[0][1],
                                        corners[3][0] - corners[0][0])
            midpoint = np.array(list(corners[0]), np.int32)
            midpoint[0] += midpoint_dist * np.cos(midpoint_angle)
            midpoint[1] += midpoint_dist * np.sin(midpoint_angle)
            # Put the marker code in the middle of the tag
            cv2.putText(img, str(code), (midpoint[0]-5, midpoint[1]+10),
                        cv2.FONT_HERSHEY_PLAIN, 1.5, text_color, 2)

    def rotate_mat_cw(self, mat):
        """Rotates the matrix clockwise.
        Given:

        [[1, 1, 0, 0, 0, 0],
         [0, 0, 1, 0, 0, 0],
         [0, 0, 0, 0, 0, 1],
         [0, 0, 0, 0, 0, 0],
         [0, 0, 1, 0, 0, 0],
         [0, 0, 0, 0, 0, 1]]

        Returns:

        [[0, 0, 1, 0, 0, 1],
         [0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0],
         [0, 1, 0, 0, 1, 0],
         [1, 0, 0, 0, 0, 0],
         [1, 0, 0, 0, 0, 0]])
        """
        return np.dot(mat.T, self._flip_matrix)

    def flip_mat(self, mat):
        """
        Flips a matrix (as if it were seen in a mirror).
        Given

        [[1, 1, 0, 0, 0, 0],
         [0, 0, 1, 0, 0, 0],
         [0, 0, 0, 0, 0, 1],
         [0, 0, 0, 0, 0, 0],
         [0, 0, 1, 0, 0, 0],
         [0, 0, 0, 0, 0, 1]]

        Returns:

        [[0, 0, 0, 0, 1, 1],
         [0, 0, 0, 1, 0, 0],
         [1, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0],
         [0, 0, 0, 1, 0, 0],
         [1, 0, 0, 0, 0, 0]])
        """
        return np.dot(mat, self._flip_matrix)


