"""
Various hamming distance related code created for thesis work.
"""

import itertools

import numpy as np

def hamming_weight(x):
    # http://stackoverflow.com/questions/407587/python-set-bits-count-popcount
    return bin(x).count('1')

def hamming_dist(x, y):
    return hamming_weight(x ^ y)

def bch_divide(data, g_func, data_bit_size, gen_bit_size):
    # http://en.wikiversity.org/wiki/Reed%E2%80%93Solomon_codes_for_coders#BCH_codes
    for i in xrange(data_bit_size-1, -1, -1):
        # if msb is a zero we can ignore this calculation
        if (data >> (gen_bit_size + i)):
            data ^= g_func << i
    return data

def bch_encode(data, g_func, data_bit_size, gen_bit_size):
    # http://en.wikiversity.org/wiki/Reed%E2%80%93Solomon_codes_for_coders#BCH_codes
    shifted_data = data << gen_bit_size
    return shifted_data ^ bch_divide(shifted_data, g_func, data_bit_size, 
                                     gen_bit_size)

def get_distances(codes):
    '''Compute the hamming distances between each code.
    Return: {codeA: {codeB:4, CodeC:5, ...}, codeB: {codeC:3,...},...}'''
    distances = {}
    clen = len(codes)
    for i in xrange(clen-1):
        i_dists = {}
        for j in xrange(i+1, clen):
            i_dists[codes[j]] = hamming_dist(codes[i], codes[j])
        distances[codes[i]] = i_dists

    return distances

def get_histogram(codes):
    '''Get the distribution of distances between the different codes.'''
    hist = {}
    for i, j in itertools.combinations(xrange(len(codes)), 2):
        d = hamming_dist(codes[i][2], codes[j][2])
        hist[d] = hist.setdefault(d, 0) + 1
    return hist

def get_min_distances(codes, verbose=False):
    '''Compute the hamming distances between each code and store the minimum
    distance and code.

    The result will return rows:
    [code, rotation, message, minimum distance, minimum distance frequency]
    Return: Nx5 numpy.int64 where N is len(codes)'''
    total_codes = len(codes)
    distances = np.zeros((total_codes, 5), np.int64)
    for i in xrange(total_codes):
        if verbose:
            print i, codes[i], "--",
        distances[i, 0] = codes[i][0] # code/data
        distances[i, 1] = codes[i][1] # rotation (0, 1, 2, 3)
        distances[i, 2] = codes[i][2] # message/encoding
        min_index = -1
        min_distance = np.inf
        # Keep track of the number of same min hamming dist collisions.
        min_count = 0
        for j in xrange(total_codes):
            if i == j:
                continue
            distance = hamming_dist(codes[i][2], codes[j][2])
            if distance < min_distance:
                min_index = j
                min_distance = distance
                min_count = 0
            if distance == min_distance:
                min_count += 1
        distances[i, 3] = min_distance
        distances[i, 4] = min_count
        if verbose:
            print min_index, codes[min_index], min_distance, "===", min_count
    return distances


